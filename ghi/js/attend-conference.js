window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById("conference");

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    const loadingSpinner = document.getElementById('loading-conference-spinner')
    loadingSpinner.classList.add('d-none')
    selectTag.classList.remove('d-none')
    }

    const attendeeForm = document.getElementById('create-attendee-form')
    attendeeForm.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(
            "http://localhost:8001/api/attendees/",
            fetchConfig
            );
        if (response.ok) {
            attendeeForm.reset();
            const newAttendee = await response.json();
            const successMessage = document.getElementById('success-message')
            successMessage.classList.remove('d-none')
            attendeeForm.classList.add('d-none')
        }
    });
});
